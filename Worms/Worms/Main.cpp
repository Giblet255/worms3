// Loading Libary 
//-----------------------------------
#include <SFML/Graphics.hpp>
#include <vector>
#include "Vectorhelper.h"


// Worms By Scott Gilbert 02.10.21

// This is the Goof around Code 

int main()
{
	// Setting up Game window , 
	sf::RenderWindow gameWindow;
	gameWindow.create(sf::VideoMode(800, 600), "Worms By Scott Gilbert",sf::Style::Titlebar|sf::Style::Close);
	
	// -------------------------------
	//  Game Setup Start up Section
	// -------------------------------


	// Set up  Key press
	gameWindow.setKeyRepeatEnabled(false);
	
	// Setup Game Clock 
	sf::Clock gameClock;


	// Setting up Player Sprite
	sf::Texture playerTexture;
	playerTexture.loadFromFile("Assets/Graphics/player.png");
	sf::Sprite playerSprite;
	playerSprite.setTexture(playerTexture);
	
	// Setup Starting loc 
	sf::Vector2f playerPos(50, 75);
	playerSprite.setPosition(playerPos);
	sf::Vector2f newPosition(playerPos);
	
	// setting player Cirle
	float PlayerRad = 15;
	sf::CircleShape Playershape(15);
	Playershape.setRadius(PlayerRad);

	// set the shape color to green
	Playershape.setFillColor(sf::Color(100, 250, 50));

	// setting up Bounding box / sphear 
	 
	
	// ******************************************************
	// Setting up Player2 Sprite
	sf::Texture player2Texture;
	player2Texture.loadFromFile("Assets/Graphics/player2.png");

	sf::Sprite player2Sprite;
	player2Sprite.setTexture(player2Texture);

	// Setup Starting loc 
	sf::Vector2f player2Pos(655, 381);
	player2Sprite.setPosition(player2Pos);

	// setting player2 Cirle
	float Player2rad = 15;
	sf::CircleShape Player2shape(15);
	Player2shape.setRadius(Player2rad);

	// set the shape color to green
	Player2shape.setFillColor(sf::Color(100, 250, 50));
	Player2shape.setPosition(player2Pos);

	// Load texture
	sf::Texture PauseTexture;
	PauseTexture.loadFromFile("Assets/Graphics/pause.png");

	// PauseScreen Sprite Sprite
	sf::Sprite PauseSprite;
	PauseSprite.setTexture(PauseTexture);
	sf::Vector2f PausePosition = sf::Vector2f(400.0f, -100.0f);
	PauseSprite.setPosition(PausePosition);
	PauseSprite.setOrigin(PauseTexture.getSize().x / 2, PauseTexture.getSize().y / 2);

	// Interpolation data
	sf::Vector2f begin = PausePosition;
	sf::Vector2f end = PausePosition;
	sf::Vector2f change = end - begin;
	float duration = 1.0f;
	float time = 0.0f;
	bool toggle = true;

	// Bomb Setup 
	sf::Texture bombTexture;
	bombTexture.loadFromFile("Assets/Graphics/bomb.png");
	sf::Sprite bombSprite;
	bombSprite.setTexture(bombTexture);
	sf::Vector2f bombVel(0.0f, 0.0f);
	// Pips Setup


	// setting the Vector od pips up 
	std::vector <sf::Sprite> Pips;
	sf::Texture playerPipTexture;
	playerPipTexture.loadFromFile("Assets/Graphics/pip.png");
	int numPips = 10;
	for (int i = 0; i < numPips; ++i)
	{
		Pips.push_back(sf::Sprite());
		Pips[i].setTexture(playerPipTexture);
	}

	// Pips Vars 
	sf::Vector2f pipGravity(0, 1000.0f);
	sf::Vector2f firingPos(300.0f, 300.0f);
	sf::Vector2f firingDir(1.0f, 0);
	sf::Vector2f firingVel(0.0f, 0.0f);
	float firingSpeed = 750.0f;


	// BackGound img ------------------------
	sf::Texture bgTexture;
	bgTexture.loadFromFile("Assets/Graphics/BG.png");
	sf::Sprite BGImg;
	BGImg.setTexture(bgTexture);
	BGImg.setPosition(gameWindow.getSize().x / 2 - bgTexture.getSize().x / 2,
	gameWindow.getSize().y / 2 - bgTexture.getSize().y / 2);


	// Setting up the Ground PLatforms

	sf::Texture platformTexture;
	platformTexture.loadFromFile("Assets/Graphics/Ground.png");
	const int Num_platforms = 2;
	std::vector<sf::Sprite> platformSprite;
	for (int i = 0; i < Num_platforms; ++i)
	{
		platformSprite.push_back(sf::Sprite());
		platformSprite[i].setTexture(platformTexture);
		platformSprite[i].setPosition(i * 600,400 + i*50);
	}


   // Setting up Crates 

	sf::Texture createTexture;
	createTexture.loadFromFile("Assets/Graphics/box.png");
	const int Num_creates = 2;
	std::vector<sf::Sprite> createSprite;
	for (int i = 0; i < Num_creates; ++i)
	{
		createSprite.push_back(sf::Sprite());
		createSprite[i].setTexture(createTexture);
		createSprite[i].setPosition((i * 85)+650, 410);
	}


	// -------------------------------
	//  Game Loop
	// -------------------------------

	while (gameWindow.isOpen())
	{
		// -----------------------------------------------
	    //  Input Section 
	    // -----------------------------------------------

		sf::Event event;
		while (gameWindow.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				gameWindow.close();

			// Is the event a key press event
			if (event.type == sf::Event::KeyPressed)
			{
				// Is the event specifically related to the escape key
				if (event.key.code == sf::Keyboard::Escape)
				{
					gameWindow.close();
				}
			}
		}


		// Setting up movment Vars 
		sf::Vector2f velocity;
		float speed; 
		sf::Vector2f Gravity(0, 300.0f);
		
		
		
		sf::Vector2f previousPosition;

		speed = 300.0f;
		
		
		
		
		

		if (toggle == true)
		{

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			{
				// Move player left
				velocity.x = -speed;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			{
				// Move player right
				velocity.x = speed;
			}
		

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
			{
				

					const float JUMP_VALUE = -900; // negative to go up. Adjust as needed.
					velocity.y = JUMP_VALUE - Gravity.y;
			}

			//if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
			//{
				// Throw Bomb
				//bombSprite.setPosition(newPosition);
				//bombVel = firingVel;
			//}

			if (event.type == sf::Event::MouseButtonPressed)
			{
				if (event.mouseButton.button == sf::Mouse::Left)
				{
					// Throw Bomb
					bombSprite.setPosition(newPosition);
					bombVel = firingVel;
				}
			}
			
			
			velocity.y = velocity.y + Gravity.y;
		}
		else
		{

		}

			
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::P))
			{

				// If P is Clicked... Show Pause Menu
				begin = PausePosition;
				//end = sf::Vector2f(sf::Mouse::getPosition(window));
				end = sf::Vector2f(PausePosition = sf::Vector2f(400.0f, 200.0f));
				change = end - begin;

				time = 0;
				toggle = false;


			}


			if (toggle == false);
			{
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::O))
				{

					// If O is Clicked... Hide Pause Menu
					begin = PausePosition;
					//end = sf::Vector2f(sf::Mouse::getPosition(window));
					end = sf::Vector2f(PausePosition = sf::Vector2f(400.0f, -100.0f));
					change = end - begin;

					time = 0;

					toggle = true;
				}

			}
			

			

		// -----------------------------------------------
		// Update Section
		// -----------------------------------------------
			

		sf::Time frameTime = gameClock.restart();
		float deltaTime = frameTime.asSeconds();

		previousPosition = playerSprite.getPosition();

		

		// Calculate the new position
		newPosition = playerSprite.getPosition() + velocity * frameTime.asSeconds();

		// Calculate The Grav 
		//calculate the new velocity
		
		sf::Vector2f player2Pos = player2Sprite.getPosition();


		playerSprite.setPosition(newPosition);
		Playershape.setPosition(newPosition);


		// SCOTT YOU ARE Going to need this code for graded unit it will work out pathing its uses to work out if 2 curcale intersect 
		
		sf::Vector2f circledisplacment = Playershape.getPosition() - Player2shape.getPosition(); 
		float SQUDistance = circledisplacment.x * circledisplacment.x + circledisplacment.y * circledisplacment.y;
		float SQUDRad = (PlayerRad + Player2rad) * (PlayerRad + Player2rad);

		bool collied = SQUDistance < SQUDRad;


		if (collied)
		{
			Playershape.setFillColor(sf::Color(250, 100, 50));

			// added collsion form left to right
			if (Player2shape.getPosition().x > Playershape.getPosition().x)
			{
				//Playershape.setFillColor(sf::Color(20, 20, 20));
				newPosition.x = Playershape.getPosition().x - 14;
				newPosition.y = Playershape.getPosition().y ;

				playerSprite.setPosition(newPosition);
				Playershape.setPosition(newPosition);


			}

			// Add collsion form right to left

			if (Player2shape.getPosition().x < Playershape.getPosition().x)
			{
				//Playershape.setFillColor(sf::Color(20, 20, 20));
				newPosition.x = Playershape.getPosition().x + 14;
				newPosition.y = Playershape.getPosition().y;

				playerSprite.setPosition(newPosition);
				Playershape.setPosition(newPosition);


			}



		}
		else
		{
			Playershape.setFillColor(sf::Color(100, 250, 50));
		}


		
		

		// Interpolate over time
		if (time < duration)
		{
			// Linear Interpolation
			//sf::Vector2f k1 = change / duration;
			//sf::Vector2f k2 = begin;
			//PausePosition = k1 * time + k2;

			// Quad Ease In
			sf::Vector2f k1 = change / (duration * duration);
			sf::Vector2f k2 = sf::Vector2f(0.0f, 0.0f);
			sf::Vector2f k3 = begin;
			PausePosition = k1 * time * time + k2 * time + k3;

			time += deltaTime;
		}
		else
		{
			// We finished the duration, move us instantly.
			// Instant movement
			PausePosition = end;

		}
		

		// Setting up Vars For PIPS 

		float PipTime = 0;

		

		// this is finding mouse pos 

		sf::Vector2f mousePossition = sf::Vector2f(sf::Mouse::getPosition(gameWindow));
		firingDir = mousePossition - firingPos;
		
		// Normailsing fireing direction to size 1(unit vector)
		float mag = sqrt(firingDir.x * firingDir.x + firingDir.y * firingDir.y);

		firingDir /= mag;

		 firingVel = firingDir * firingSpeed;


		for (int i = 0; i < Pips.size(); i++)
		{
			PipTime += 0.1;
			sf::Vector2f pipPosition;
			pipPosition = 0.5f * pipGravity * PipTime * PipTime
				+ firingVel * PipTime + newPosition;
			Pips[i].setPosition(pipPosition);

			
		}

		// Update Bomb position
		bombVel += pipGravity * frameTime.asSeconds();
		bombSprite.setPosition(bombSprite.getPosition() + bombVel * frameTime.asSeconds());
        
		// Collsion for player on player form 
		for (int i = 0; i < platformSprite.size(); ++i)
		{
			sf::FloatRect platormRect = platformSprite[i].getGlobalBounds();
			sf::FloatRect playerRect = playerSprite.getGlobalBounds();

			if (playerRect.intersects(platormRect))
			{
				// find side 
				sf::Vector2f depth = CollisionDepth(platormRect, playerRect);
				sf::Vector2f absDepth = sf::Vector2f(abs(depth.x), abs(depth.y));

				if (absDepth.x < absDepth.y)
				{
					// Move out of Collision
					sf::Vector2f currentPlayerPos = playerSprite.getPosition();
					currentPlayerPos.x += depth.x;
					playerSprite.setPosition(currentPlayerPos);

					if (depth.x > 0)
					{
						
						currentPlayerPos.x -= depth.x;
						playerSprite.setPosition(currentPlayerPos);
					}


				}
				else
				{
					sf::Vector2f currentPlayerPos = playerSprite.getPosition();
					currentPlayerPos.y -= depth.y ;
					playerSprite.setPosition(currentPlayerPos);
				}
			}

		}


		// check collsion Creates For Bomb
		for (int i = 0; i < createSprite.size(); ++i)
		{
			sf::FloatRect crateRect = createSprite[i].getGlobalBounds();
			sf::FloatRect bombRect = bombSprite.getGlobalBounds();

			if (crateRect.intersects(bombRect))
			{
				// Bounce

				// find side 
				sf::Vector2f depth = CollisionDepth(bombRect, crateRect);
				sf::Vector2f absDepth = sf::Vector2f(abs(depth.x), abs(depth.y));
				sf::Vector2f normal;
				if (absDepth.x < absDepth.y)
				{
					// Move out of Collision
					sf::Vector2f BombPos = bombSprite.getPosition();
					BombPos.x += depth.x;
					bombSprite.setPosition(BombPos);

					if (depth.x > 0)
					{
						// Set normail Vection
						normal = sf::Vector2f(-1, 0);
						BombPos.x -= depth.x;
						bombSprite.setPosition(BombPos);
					}
					else
					{
						// Set normail Vection
						normal = sf::Vector2f(-1, 0);
					}


				}
				else
				{
					// move out collsion 
					sf::Vector2f BombPos = bombSprite.getPosition();
					BombPos.y += depth.y;
					bombSprite.setPosition(BombPos);

					// Are we Collising for top or bottom 
					if (depth.y > 0)
					{
						normal = sf::Vector2f(0, -1);
					}
					else
					{
						normal = sf::Vector2f(0, 1);
					}
				}


				// Apply Reflectio Exqation 
				// R=I-2N(I*N)
				// R = out oing Velcity
				// I = incomiing Velicity
				// N = Normail 

				bombVel = bombVel - 2.0f * normal * (VectorDot(bombVel, normal));

			} 



			// check collsion Platforms For Bomb
			for (int i = 0; i < platformSprite.size(); ++i)
			{
				sf::FloatRect crateRect = platformSprite[i].getGlobalBounds();
				sf::FloatRect bombRect = bombSprite.getGlobalBounds();

				if (crateRect.intersects(bombRect))
				{
					// Bounce

					// find side 
					sf::Vector2f depth = CollisionDepth(bombRect, crateRect);
					sf::Vector2f absDepth = sf::Vector2f(abs(depth.x), abs(depth.y));
					sf::Vector2f normal;
					if (absDepth.x < absDepth.y)
					{
						// Move out of Collision
						sf::Vector2f BombPos = bombSprite.getPosition();
						BombPos.x += depth.x;
						bombSprite.setPosition(BombPos);

						if (depth.x > 0)
						{
							// Set normail Vection
							normal = sf::Vector2f(-1, 0);
							BombPos.x -= depth.x;
							bombSprite.setPosition(BombPos);
						}
						else
						{
							// Set normail Vection
							normal = sf::Vector2f(-1, 0);
						}


					}
					else
					{
						// move out collsion 
						sf::Vector2f BombPos = bombSprite.getPosition();
						BombPos.y += depth.y;
						bombSprite.setPosition(BombPos);

						// Are we Collising for top or bottom 
						if (depth.y > 0)
						{
							normal = sf::Vector2f(0, -1);
						}
						else
						{
							normal = sf::Vector2f(0, 1);
						}
					}


					// Apply Reflectio Exqation 
					// R=I-2N(I*N)
					// R = out oing Velcity
					// I = incomiing Velicity

					bombVel = bombVel - 2.0f * normal * (VectorDot(bombVel, normal));

				}
			}
			
		}
		// Update sprite to correct position
		PauseSprite.setPosition(PausePosition);


		// -----------------------------------------------
		// Draw Section
		// -----------------------------------------------
		// Clear the window to a single colour
		gameWindow.clear(sf::Color(85, 30, 138));
		// Drawing BackGound Image 
		gameWindow.draw(BGImg);

		// Draw the Platorms 
		for (int i = 0; i < platformSprite.size(); ++i)
		{
			gameWindow.draw(platformSprite[i]);

		}

		gameWindow.draw(Playershape);
		gameWindow.draw(playerSprite);
		
		gameWindow.draw(Player2shape);
		gameWindow.draw(player2Sprite);

		// Draw the Creates 
		for (int i = 0; i < createSprite.size(); ++i)
		{
			gameWindow.draw(createSprite[i]);

		}

		// Draw the Pips 
		for (int i = 0; i < Pips.size(); ++i)
		{
			gameWindow.draw(Pips[i]);

		}
		
		
		gameWindow.draw(bombSprite);

		gameWindow.draw(PauseSprite);

		gameWindow.display();

	} // End of Game loop
	

	return 0;
}